import root from '../index'

import './index.scss'

const KEY = 'LABEL_NAME'

root({
  redirectUrl: `${process.env.LN_APP_PATH}/${KEY}`,
})
