const inquirer = require('inquirer')
const createConfig = require('../helpers/config').createConfig

const questions = [
  {
    type: 'input',
    name: 'staticPath',
    message: 'Informe o path para o projeto estático:',
    validate: input => {
      if (typeof input !== 'string' || input.length === 0) {
        return 'Você precisa fornecer um path'
      }

      return true
    },
  },
  {
    type: 'input',
    name: 'reactPath',
    message: 'Informe o path para o projeto React:',
    validate: input => {
      if (typeof input !== 'string' || input.length === 0) {
        return 'Você precisa fornecer um path'
      }

      return true
    },
  },
]

module.exports = function config() {
  inquirer
    .prompt(questions)
    .then(options => {
      createConfig(options.staticPath, options.reactPath)
    })
}