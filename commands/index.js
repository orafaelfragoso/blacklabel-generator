const config = require('./config')
const generate = require('./generate')
const remove = require('./remove')

module.exports = {
  config,
  generate,
  remove,
}