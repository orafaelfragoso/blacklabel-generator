const path = require('path')
const inquirer = require('inquirer')
const configHelper = require('../helpers/config')
const filesHelper = require('../helpers/files')

const questions = [
  {
    type: 'input',
    name: 'fullName',
    message: 'Qual o nome (formatado e com acentos) da label? (Ex.: Itaú):',
    validate: input => {
      if (typeof input !== 'string' || input.length === 0) {
        return 'Você precisa fornecer um nome!'
      }

      return true
    },
  },
  {
    type: 'input',
    name: 'color',
    message: 'Qual a cor da label? (Ex.: #fff ou #f1f1f1):',
    validate: input => {
      if (typeof input !== 'string' || input.length === 0) {
        return 'Você precisa fornecer a cor da label!'
      }

      if (!/^#([0-9A-F]{3}){1,2}$/i.test(input)) {
        return 'Você precisa fornecer uma cor válida!'
      }

      return true
    },
  },
  {
    type: 'list',
    name: 'pronoun',
    default: 'o',
    message: 'A marca é chamada de forma feminina ou masculina?:',
    choices: [
      { name: 'Masculino', value: 'o' },
      { name: 'Feminino', value: 'a' },
    ],
  },
  {
    type: 'input',
    name: 'imagesPath',
    message: 'Qual o path das imagens da label?',
    validate: input => {
      if (typeof input !== 'string' || input.length === 0) {
        return 'Você precisa fornecer um caminho!'
      }

      return true
    },
  },
]

module.exports = function generate(label) {
  inquirer
    .prompt(questions)
    .then(async (options) => {
      const config = configHelper.loadConfig()
      const paths = configHelper.getPaths()
      const labelFolder = `${config.staticPath}${paths.static.pages}/${label}`
      const configFolder = `${config.staticPath}/config`
      const partnersFolder = `${config.staticPath}/src/partials/partners`

      // criar pasta no projeto com nome da label (se não existir)
      filesHelper.copyFolder(
        path.resolve(__dirname + '/../templates/static/label'),
        labelFolder,
      )

      // Renomear handlebars da label
      filesHelper.renameFile(`${labelFolder}/LABEL_NAME.hbs`, `${labelFolder}/${label}.hbs`)

      // substituir o conteudo dos arquivos de template pela label
      filesHelper.replaceInFile(`${labelFolder}/index.hbs`, 'LABEL_NAME', label)
      filesHelper.replaceInFile(`${labelFolder}/index.js`, 'LABEL_NAME', label)
      filesHelper.replaceInFile(`${labelFolder}/index.scss`, 'LABEL_NAME', label)
      filesHelper.replaceInFile(`${labelFolder}/index.scss`, 'LABEL_COLOR', options.color)
      filesHelper.replaceInFile(`${labelFolder}/${label}.hbs`, 'LABEL_NAME', label)
      filesHelper.replaceInFile(`${labelFolder}/${label}.hbs`, 'LABEL_FULL_NAME', options.fullName)
      filesHelper.replaceInFile(`${labelFolder}/${label}.hbs`, 'LABEL_PRONOUN', options.pronoun)
      filesHelper.replaceInFile(`${labelFolder}/${label}.hbs`, 'LABEL_COLOR', options.color)

      // mover as imagens da label para o projeto
      filesHelper.copyFolder(
        options.imagesPath,
        `${labelFolder}/images`,
      )

      // adicionar os imports nos lugares corretos
      filesHelper.injectWebpackConfig(configFolder, label)
      filesHelper.injectWebpackDevServer(configFolder, label)
      filesHelper.injectPartners(partnersFolder, label)
    })
}