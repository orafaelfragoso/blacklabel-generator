const configHelper = require('../helpers/config')
const filesHelper = require('../helpers/files')

module.exports = function remove(label) {
  const config = configHelper.loadConfig()
  const paths = configHelper.getPaths()
  const configFolder = `${config.staticPath}/config`
  const partnersFolder = `${config.staticPath}/src/partials/partners`

  filesHelper.deleteFolder(`${config.staticPath}${paths.static.pages}/${label}`)
  filesHelper.removeWebpackConfig(configFolder, label)
  filesHelper.removeWebpackDevServer(configFolder, label)
  filesHelper.removePartners(partnersFolder, label)
}
