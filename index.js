#!/usr/bin/env node

const commander = require('commander')
const commands = require('./commands')
const program = new commander.Command()

program
  .version('0.0.1')
  .description('Gerador de black labels para o LNO.')

program
  .command('config')
  .alias('c')
  .description('Gera um arquivo de configuração com a localização dos projetos.')
  .action(commands.config)

program
  .command('generate <name>')
  .alias('g')
  .description('Gera uma nova black label a partir dos parâmetros.')
  .action(commands.generate)

program
  .command('remove <name>')
  .alias('r')
  .description('Remove uma black label a partir dos parâmetros.')
  .action(commands.remove)

program.on('command:*', () => {
  console.error('Comando inválido: %s\nVeja --help para uma lista de comandos disponíveis.', program.args.join(' '));
  process.exit(1);
})

program.parse(process.argv)