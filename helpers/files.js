const fs = require('fs-extra')

const createFolder = (path) => {
  try {
    fs.mkdirSync(path, { recursive: true })
  } catch (err) {
    if (err.code === 'EEXIST') {
      return null
    } else {
      console.log(err)
    }
  }
}

const copyFolder = (from, to) => {
  try {
    fs.copySync(from, to)
  } catch (error) {
    console.log(error)
  }
}

const deleteFolder = (path) => {
  try {
    fs.removeSync(path)
  } catch (err) {
    console.log(err)
  }
}

const renameFile = (oldPath, newPath) => {
  try {
    fs.renameSync(oldPath, newPath)
  } catch (error) {
    console.log(error)
  }
}

const replaceInFile = (path, needle, haystack) => {
  try {
    const file = fs.readFileSync(path, 'utf8')
    const newFileContent = file.replace(new RegExp(needle, 'g'), haystack)

    fs.writeFileSync(path, newFileContent)
  } catch (error) {
    console.log(error)
  }
}

const injectWebpackConfig = (path, label) => {
  const filePath = `${path}/webpack.config.prod.js`
  const regex = /.+new\sExtractTextPlugin\(\{(.*\n){3}/g
  const template = `$&    new StyleExtHtmlWebpackPlugin({
      chunks: ['${label}'],
      minify: true
    }),\n`

  try {
    replaceInFile(filePath, regex, template)
  } catch (error) {
    console.log(error)
  }
}

const removeWebpackConfig = (path, label) => {
  try {
    const filePath = `${path}/webpack.config.prod.js`
    const file = fs.readFileSync(filePath, 'utf8')
    const regex = new RegExp(`(.*[\\r\\n])(.+chunks:\\s\\[\\'${label}\\'\\],)(.*\\n){3}`, 'g')
    const newFileContent = file.replace(regex, '')

    fs.writeFileSync(filePath, newFileContent)
  } catch (error) {
    console.log(error)
  }
}

const injectWebpackDevServer = (path, label) => {
  const filePath = `${path}/webpackDevServer.config.js`
  const regex = /.+rewrites:\s\[/g
  const template = `$&\n        { from: /^\\/parceiros\\/${label}/, to: '${label}.html' },`

  try {
    replaceInFile(filePath, regex, template)
  } catch (error) {
    console.log(error)
  }
}

const removeWebpackDevServer = (path, label) => {
  try {
    const filePath = `${path}/webpackDevServer.config.js`
    const file = fs.readFileSync(filePath, 'utf8')
    const regex = new RegExp(`.+${label}.*\\n`, 'g')
    const newFileContent = file.replace(regex, '')

    fs.writeFileSync(filePath, newFileContent)
  } catch (error) {
    console.log(error)
  }
}

const injectPartners = (path, label) => {
  const filePath = `${path}/partners.hbs`
  const regex = /.+\"ln_partners--block\">/g
  const template = `$&\n  {{#> partner key="${label}" blacklabel="${label}"}}
  {{> ln-img src="https://www.serasaconsumidor.com.br/lno/static-webfiles/partners/${label}@1x.png"
    retina="https://www.serasaconsumidor.com.br/lno/static-webfiles/partners/${label}@2x.png"
    alt="${label}"
    class="ln_partner--img"}}
  {{/partner}}\n`

  try {
    replaceInFile(filePath, regex, template)
  } catch (error) {
    console.log(error)
  }
}

const removePartners = (path, label) => {
  try {
    const filePath = `${path}/partners.hbs`
    const file = fs.readFileSync(filePath, 'utf8')
    const regex = new RegExp(`(.+blacklabel=\\"${label}\\".*\\n)(.*\\n){6}`, 'g')
    const newFileContent = file.replace(regex, '')

    fs.writeFileSync(filePath, newFileContent)
  } catch (error) {
    console.log(error)
  }
}

module.exports = {
  createFolder,
  copyFolder,
  deleteFolder,
  renameFile,
  replaceInFile,
  injectPartners,
  injectWebpackConfig,
  injectWebpackDevServer,
  removeWebpackConfig,
  removeWebpackDevServer,
  removePartners,
}