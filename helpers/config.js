const fs = require('fs')
const os = require('os')

const paths = {
  static: {
    pages: '/src/pages/blacklabel',
    devServer: '/config/webpackDevServer.config.js',
    prod: '/config/webpack.config.prod.js',
    partners: '/src/partials/partners/partners.hbs',
  },
  react: {

  }
}

const readConfigFile = () => {
  const file = `${os.homedir()}/.blacklabel.json`
  return fs.readFileSync(file, 'utf-8')
}

const createConfig = (staticPath, reactPath) => {
  try {
    readConfigFile()
  } catch (err) {
    const contents = JSON.stringify({
      staticPath: staticPath,
      reactPath: reactPath,
    })
  
    fs.writeFile(file, contents, { flag: 'wx' }, (error) => {
      if (error) throw error
      console.log('Arquivo de configuração gerado!')
    })
  }
}

const loadConfig = () => {
  try {
    return JSON.parse(readConfigFile())
  } catch (error) {
    console.log('O arquivo de configuração não existe ou possuí erros.')
    throw error
  }
}

const getPaths = () => {
  return paths
}

module.exports = {
  createConfig,
  loadConfig,
  getPaths,
}